# Workspaces for python

## Workspaces in Rust:

https://doc.rust-lang.org/cargo/reference/workspaces.html

"A workspace is a collection of one or more packages, called workspace members, that are managed together."

## Workspaces in Javascript:

https://docs.npmjs.com/cli/v9/using-npm/workspaces?v=true

"Workspaces is a generic term that refers to the set of features in the npm cli that provides support to managing multiple packages from your local file system from within a singular top-level, root package."

https://classic.yarnpkg.com/lang/en/docs/workspaces/

"Your dependencies can be linked together, which means that your workspaces can depend on one another while always using the most up-to-date code available."

## Workspaces in PHP:

https://getcomposer.org/doc/05-repositories.md#path

Although not explicitly mentioned as "workspaces", PHP has means to allow local developement of multiple interdependent packages:

"In addition to the artifact repository, you can use the path one, which allows you to depend on a local directory, either absolute or relative. This can be especially useful when dealing with monolithic repositories."

The term "monolithic repository" even has an abbreviation: "Monorepo". This is a definition from Atlassian:

https://www.atlassian.com/git/tutorials/monorepos

"

- The repository contains more than one logical project (e.g. an iOS client and a web-application)
- These projects are most likely unrelated, loosely connected or can be connected by other means (e.g via *dependency management* tools).The repository is large in many ways:
    + Number of commits
    + Number of branches and/or tags
    + Number of files tracked
    + Size of content tracked (as measured by looking at the .git directory of the repository)

" 

So "workspaces" are an option to split up monorepos into smaller repos that are used as packages at the same time.

## My interpretation for python

Manage a set of local packages under one root package. Instead of developing one big package, use "workspaces" to split your application locally into multiple packages but with the advantage of being able to edit them and see the changes in the root package immediately. The root package depends on local packages which may have their own dependencies. 

### Assumptions

Currently, there is no known standard mechanism in python to support "workspaces". There is a package and dependency manager called "pip". Packages may be installed in "editable" mode so that changes are reflected immediately. There is no known way to activate editable mode on dependencies. Instead, local packages have to be installed in editable mode via the pip CLI before installation of other packages that depend on them. The dependency string format in the package metadata file pyproject.toml does not support relative paths.  

### Suggested solution

Read metadata from pyproject.toml in local packages. Install local packages in correct order in editable mode. Use a small script for that. 

## Usage

Create structure as shown in this repository. Create virtual environment with `python -m venv venv`.  Run `python install_editable_workspaces.py`. Run `pip install .`. The root package will use the already installed, editable local packages.

The local packages may be published like normal packages but "workspaces" allows you to "simulate" packages locally without having to publish them every time there was a change.

## Possible improvements

The local packages might be stored in an external version control system. Provide setup means to read the local packages from the root pyproject.toml, download them automatically into the `workspaces` folder and subsequently run the `install_editable_workspaces.py`. An additional file might indicate which packages should be handled as local packages. The file may also contain GIT URLs to download the packages.
