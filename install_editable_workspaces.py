import tomli
import graphlib
import os
import sys
import subprocess 

graph = {}
local_packages = {}
workspaces = os.listdir("workspaces")
workspaces_metadata = {}
local_packages_path = {}
local_packages = set()

for workspace in workspaces:
  workspace_path = 'workspaces/' + workspace
  metadata_file = workspace_path + '/pyproject.toml'
  with open(metadata_file, 'rb') as f:
    metadata = tomli.load(f)
    workspaces_metadata[workspace] = metadata
    local_package = metadata['project']['name']
    local_packages.add(local_package)
    local_packages_path[local_package] = workspace_path

print(f"Found local packages: {', '.join(local_packages)}")

for workspace, metadata in workspaces_metadata.items():
  if 'dependencies' in metadata['project']:
     dependencies = metadata['project']['dependencies']
     interdependencies = set()
     for dependency in dependencies:
     	if dependency in local_packages:
     	  interdependencies.add(dependency)
     local_package_name = metadata['project']['name']
     graph[local_package_name] = interdependencies
     print(f"Interdependencies for local package '{local_package_name}': {', '.join(list(interdependencies))}")

ts = graphlib.TopologicalSorter(graph)
topological_order = tuple(ts.static_order())
print(f"Calculated installation order for local packages: {', '.join(topological_order)}")
for local_package in topological_order:
 print(f"Installing '{local_package}' as editable package")
 path = local_packages_path[local_package]
 subprocess.run([sys.executable, '-m', 'pip', 'install', '--editable', path], check=True)


